package transaction

import (
	"encoding/hex"
	"fmt"
	"lurcury/crypto"
	"lurcury/db"
	"lurcury/params"
	"lurcury/types"
	"math/big"
	"os"
	"strings"
)

func BoltChange(core_arg *types.CoreStruct, Transaction types.TransactionJson) (bool, string) {

	var address string
	switch Transaction.Crypto {
	case "cic":
		address = crypto.CICKeyToAddress_hex(Transaction.PublicKey)
	case "secp256k1":
		address = crypto.CICKeyToAddress_hex(Transaction.PublicKey)
	case "eddsa":
		address = crypto.KeyToAddress_hex(Transaction.PublicKey)
	default:
		address = crypto.CICKeyToAddress_hex(Transaction.PublicKey)
	}

	if len(Transaction.To) < 30 {
		inter := db.StringHexGet(core_arg.NameDb, Transaction.To) //, &inter)
		Transaction.To = string(inter)
	}

	fromAccountInfo := db.AccountHexGet(core_arg.Db, address)

	if Transaction.Nonce > fromAccountInfo.Nonce {
		return false, "nonce too high"
	}
	if Transaction.Nonce < fromAccountInfo.Nonce {
		return false, "nonce too low"
	}
	tokenName := strings.Split(Transaction.Input, "$")
	if len(tokenName) != 3 {
		return false, "params number must be 3"
	}
	exchangeRate := tokenName[2]

	// Bolt change balance
	tokenNameFrom := Transaction.Out[0].Token   // ex: usdtn
	changeBalance := Transaction.Out[0].Balance // ex: 10000000000(satoshi)  (100 usdtn)
	fmt.Println("[bolt] tokenNameFrom: ", tokenNameFrom)
	fmt.Println("[bolt] changeBalance: ", changeBalance)
	receiveBalance := new(big.Float)
	smallPartBalance := new(big.Float)
	changeBalance_float := new(big.Float)
	changeBalance_float.SetString(changeBalance)

	Transaction.Type = "ttn"
	Transaction.Input = "go"
	fmt.Println("@@@@@@@@@@ Transaction.Nonce: ", Transaction.Nonce)
	fmt.Println("@@@@@@@@@@ Transaction.From.Nonce: ", db.AccountHexGet(core_arg.Db, Transaction.From).Nonce)
	Transaction.Nonce = db.AccountHexGet(core_arg.Db, Transaction.From).Nonce

	// Must do first, solve sync issue, can't wait to pendingTransaction, will cause "token not enough" error
	if tokenName[1] != "ttn" {
		res1, err := VerifyTokenTransactionBalanceAndNonce(*core_arg, Transaction)
		if res1 == false {
			fmt.Println("Tx1 res1 == false")
			fmt.Println("err: ", err)
			return false, err
		}
	}

	// Tx: MCC項目方 Sub 500 MCC, Add 500 MCC to User
	var s types.SignBodys_Struct
	const prec = 200
	receiveBalance.SetPrec(prec)
	rate := new(big.Float)
	ethWei := new(big.Float)
	ethWei.SetString("10000000000") // 10^10 (omni[10^8] -> ethwei[10^18] )
	rate.SetString(exchangeRate)    // ex: 5
	receiveBalance.Mul(changeBalance_float, rate)
	receiveBalance.Mul(receiveBalance, ethWei)
	s.Type = "ttn"
	s.Fee = params.Chain().Version.Eleve["dev"].Fee.String()
	s.Address = Transaction.From
	s.Out = []types.TransactionOut{
		types.TransactionOut{
			Token:   tokenName[1], // MCC or TTN
			Balance: receiveBalance.Text('f', 0),
		}}
	s.Balance = "0"
	s.Nonce = db.AccountHexGet(core_arg.Db, Transaction.To).Nonce
	s.Input = ""
	// 項目方PrivateKey
	// s1.PrivateKey = os.Getenv("MCC" + "PRI")
	s.PrivateKey = os.Getenv(strings.ToUpper(tokenName[1]) + "PRI") // "MCC" + "PRI" or "TTN"+"PRI"
	s.Crypto = "cic"
	s.Protocol = nil
	secondTransaction := Transactiontest(s)
	// res2, err := VerifyTokenTransactionBalanceAndNonce(*core_arg, secondTransaction)
	// if res2 == false {
	// 	fmt.Println("res2 == false")
	// 	return false, err
	// }

	// Tx: MCC項目方Usdt 1% -> Our Usdt address
	var t types.SignBodys_Struct
	smallrate := new(big.Float)
	smallrate.SetString("0.01") // 1%
	smallPartBalance.Mul(changeBalance_float, smallrate)
	t.Type = "ttn"
	t.Fee = params.Chain().Version.Eleve["dev"].Fee.String()
	t.Address = params.Chain().Version.Eleve["dev"].UsdtAddress
	t.Out = []types.TransactionOut{
		types.TransactionOut{
			Token:   tokenName[0],
			Balance: smallPartBalance.Text('f', 0),
		}}
	t.Balance = "0"
	t.Nonce = db.AccountHexGet(core_arg.Db, Transaction.To).Nonce + 1 // Continue after secondtransaction, so need to add 1
	t.Input = ""
	// 項目方PrivateKey
	t.PrivateKey = os.Getenv(strings.ToUpper(tokenName[1]) + "PRI") // MCCPRI or TTNPRI
	t.Crypto = "cic"
	t.Protocol = nil
	thirdTransaction := Transactiontest(t)
	// res3, err := VerifyTokenTransactionBalanceAndNonce(*core_arg, thirdTransaction)
	// if res3 == false {
	// 	fmt.Println("res3 == false")
	// 	return false, err
	// }

	// Exchange: ===============   USDT -> TTN   =============
	if tokenName[1] == "ttn" {
		// User 100 usdt -> Our usdt address
		result, err := VerifyTokenTransactionBalanceAndNonce(*core_arg, Transaction)
		if result == true {

			// Our usdt address 500 TTN -> User address
			var s1 types.SignBodys_Struct
			const prec = 200
			receiveBalance.SetPrec(prec)
			rate := new(big.Float)
			ethWei := new(big.Float)
			ethWei.SetString("10000000000") // 10^10 (omni[10^8] -> ethwei[10^18] )
			rate.SetString(exchangeRate)    // ex: 5
			receiveBalance.Mul(changeBalance_float, rate)
			receiveBalance.Mul(receiveBalance, ethWei)

			s1.Type = "ttn"
			s1.Fee = params.Chain().Version.Eleve["dev"].Fee.String()
			s1.Address = Transaction.From
			s1.Out = nil
			s1.Balance = receiveBalance.Text('f', 0)
			s1.Nonce = db.AccountHexGet(core_arg.Db, Transaction.To).Nonce
			s1.Input = ""
			s1.PrivateKey = os.Getenv(strings.ToUpper(tokenName[0]) + "PRI") // "USDTN"+ "PRI"
			s1.Crypto = "cic"
			s1.Protocol = nil
			ttnTransaction := Transactiontest(s1)

			ttnTransaction.Tx = hex.EncodeToString(crypto.Keccak256([]byte(EncodeForSign(secondTransaction))))
			db.TransactionHexPut(core_arg.Db, ttnTransaction.Tx, ttnTransaction)
			core_arg.PendingTransaction = append(core_arg.PendingTransaction, ttnTransaction)

			return true, "success"
		} else {

			fmt.Println("[BoltChange token = ttn]: false, err")
			return false, err
		}
	}

	core_arg.PendingTransaction = append(core_arg.PendingTransaction, Transaction)
	core_arg.PendingTransaction = append(core_arg.PendingTransaction, secondTransaction)
	core_arg.PendingTransaction = append(core_arg.PendingTransaction, thirdTransaction)

	return true, "success"
}
