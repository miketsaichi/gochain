package genesis

import (
	"fmt"
	"lurcury/account"
	"lurcury/core/block"
	"lurcury/types"
	//"math/big"
)

func InitBlock() types.BlockJson {
	b := block.NewBlock("sue",
		0,
		"fea4910f5d3e2d3af187cec5b8d8b1cfe99a9f5545ba50495bd42f4bae234b3a",
		0,
		0,
		"mogotisa",
	)
	return b
}

func InitAccount(core_arg types.CoreStruct, genesis types.BlockJson) bool {
	fmt.Println("genesis:", len(genesis.Allocate))
	for i := 0; i < len(genesis.Allocate); i++ {
		account.GenesisAccount(
			core_arg,
			genesis.Allocate[i].Address,
			genesis.Allocate[i].Amount,
			genesis.Allocate[i].TokenName,
			genesis.Allocate[i].TokenBalance,
		)
		fmt.Println(
			genesis.Allocate[i].Address,
			genesis.Allocate[i].Amount,
			genesis.Allocate[i].TokenName,
			genesis.Allocate[i].TokenBalance,
		)
	}
	return true

}

func GenesisBlock(chain string) types.BlockJson {
	var genesis types.BlockJson

	if chain == "GUC" {
		genesis = types.BlockJson{
			Version:     "sue",
			BlockNumber: 0,
			ParentHash:  "",
			Nonce:       0,
			Timestamp:   0,
			ExtraData:   "lkm",
			Hash:        "F7A5FDD2A8837B39BF7016B5D6F1CE30EF62676A1709B12A79E241C8DEC973E2",
			Allocate: []types.AllocateStruct{
				{

					Address:      "e658e4a47103b4578fd2ba6aa52af1b9fc67c129",
					Amount:       "419990000000000000000000000",
					TokenName:    []string{"gtc"},
					TokenBalance: []string{"1000000000000000000000000000"},
				},
				{

					Address: "943f1c166dcf865fcaa62409f1b02f3be25a9044",
					Amount:  "10000000000000000000000",
				},
			},
		}
	}
	if chain == "API" {
		genesis = types.BlockJson{
			Version:     "sue",
			BlockNumber: 0,
			ParentHash:  "",
			Nonce:       0,
			Timestamp:   0,
			ExtraData:   "lkm",
			Hash:        "F7A5FDD2A8837B39BF7016B5D6F1CE30EF62676A1709B12A79E241C8DEC973E2",
			Allocate: []types.AllocateStruct{
				{

					Address: "e658e4a47103b4578fd2ba6aa52af1b9fc67c129",
					Amount:  "1000000000000000000000000000",
				},
			},
		}
	}
	if chain == "WDC" {
		genesis = types.BlockJson{
			Version:     "sue",
			BlockNumber: 0,
			ParentHash:  "",
			Nonce:       0,
			Timestamp:   0,
			ExtraData:   "lkm",
			Hash:        "F7A5FDD2A8837B39BF7016B5D6F1CE30EF62676A1709B12A79E241C8DEC973E2",
			Allocate: []types.AllocateStruct{
				{

					Address: "e658e4a47103b4578fd2ba6aa52af1b9fc67c129",
					Amount:  "10000000000000000000000000000",
				},
			},
		}
	}
	if chain == "TTN" {
		genesis = types.BlockJson{
			Version:     "sue",
			BlockNumber: 0,
			ParentHash:  "",
			Nonce:       0,
			Timestamp:   0,
			ExtraData:   "lkm",
			Hash:        "F7A5FDD2A8837B39BF7016B5D6F1CE30EF62676A1709B12A79E241C8DEC973E2",
			Allocate: []types.AllocateStruct{
				{

					Address:      "9fcbadd1e0cd1099f78ac61fe9a8067654190e1a",
					Amount:       "150000000000000000000000000",
					TokenName:    []string{"btcn", "ethn", "usdtn"},
					TokenBalance: []string{"2100000000000000", "200000000000000000000000000", "600000000000000000"},
				},
			},
		}
	}
	if chain == "MOS" {
		genesis = types.BlockJson{
			Version:     "sue",
			BlockNumber: 0,
			ParentHash:  "",
			Nonce:       0,
			Timestamp:   0,
			ExtraData:   "lkm",
			Hash:        "F7A5FDD2A8837B39BF7016B5D6F1CE30EF62676A1709B12A79E241C8DEC973E2",
			Allocate: []types.AllocateStruct{
				{

					Address:      "cedc30d2cb1f48c00135ca4e86a2ad6b42a3ffff",
					Amount:       "630000000000000000000000000",
				},
			},
		}
	}
        if chain == "MT" {
		genesis = types.BlockJson{
			Version:     "sue",
			BlockNumber: 0,
			ParentHash:  "",
			Nonce:       0,
			Timestamp:   0,
			ExtraData:   "lkm",
			Hash:        "F7A5FDD2A8837B39BF7016B5D6F1CE30EF62676A1709B12A79E241C8DEC973E2",
			Allocate: []types.AllocateStruct{
				{

					Address:      "be3b51b434e300fdd46752de4a70a8f463570894",
					Amount:       "240000000000000000000000000",
				},
			},
		}
	}
        if chain == "JLT" {
		genesis = types.BlockJson{
			Version:     "sue",
			BlockNumber: 0,
			ParentHash:  "",
			Nonce:       0,
			Timestamp:   0,
			ExtraData:   "lkm",
			Hash:        "F7A5FDD2A8837B39BF7016B5D6F1CE30EF62676A1709B12A79E241C8DEC973E2",
			Allocate: []types.AllocateStruct{
				{

					Address:      "6fcaee4af5579d1d105663bd153bc9483c5e7102",
					Amount:       "900000000000000000000000000",
				},
			},
		}
	}
	if chain == "TEST" {
		genesis = types.BlockJson{
			Version:     "sue",
			BlockNumber: 0,
			ParentHash:  "",
			Nonce:       0,
			Timestamp:   0,
			ExtraData:   "lkm",
			Hash:        "F7A5FDD2A8837B39BF7016B5D6F1CE30EF62676A1709B12A79E241C8DEC973E2",
			Allocate: []types.AllocateStruct{
				// Mcc Address
				{
					Address:      "11a0a990785b12d2bc09d35ac5a31a516ae9b77e",
					Amount:       "10000000000000000000000000000",
					TokenName:    []string{"btcn", "ethn", "usdtn", "mcc"},
					TokenBalance: []string{"2100000000000000", "200000000000000000000000000", "600000000000000000", "200000000000000000000000000"},
					// TokenBalance: []string{"2100000000000000", "200000000000000000000000000", "600000000000000000", "1"},
				},
				// Fee Address
				{
					Address:      "228533c28d5b25c7d9973afd53cb57063b13fdd0",
					Amount:       "10000000000000000000000000000",
					TokenName:    []string{"btcn", "ethn", "usdtn"},
					TokenBalance: []string{"2100000000000000", "200000000000000000000000000", "600000000000000000"},
				},
				{
					Address:      "de9b5c22701fdd5f4580d766a391fdc0ba991740",
					Amount:       "10000000000000000000000000000",
					TokenName:    []string{"sdag", "btl", "etl", "usdl"},
					TokenBalance: []string{"1000000000000000000000000000000", "2100000000000000", "200000000000000000000000000", "24000000000000000000"},
				},
				{
					Address:      "23471aa344372e9c798996aaf7a6159c1d8e3eac",
					Amount:       "10000000000000000000000000000",
					TokenName:    []string{"sdag", "btl", "etl", "usdl"},
					TokenBalance: []string{"1000000000000000000000000000000", "2100000000000000", "200000000000000000000000000", "24000000000000000000"},
				},
				// Test user address
				{

					Address:      "fffa86156bb7475d8c11f63da69d74966cb4dd37",
					Amount:       "140000000000000000000000000",
					TokenName:    []string{"btcn", "ethn", "usdtn"},
					TokenBalance: []string{"2100000000000000", "200000000000000000000000000", "600000000000000000"},
				},
				// Test our usdt address
				{

					Address:      "cef49c6218ff7465e054ac25f4769c651014f482",
					Amount:       "130000000000000000000000000",
					TokenName:    []string{"btcn", "ethn", "usdtn"},
					TokenBalance: []string{"2100000000000000", "200000000000000000000000000", "600000000000000000"},
				},
			},
		}
	}
	return genesis
}
